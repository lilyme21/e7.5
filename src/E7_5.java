//////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 9/19/19
// This program will prompt the user to enter a list of
// numbers and compute an alternating sum. It will print
// the result when finished.
///////////////////////////////////////////////////////
import java.util.*;                 // importing package to use Scanner

public class E7_5 {
    public static void main(String[] args) {
        // prompting user to enter a list of integers to compute
        System.out.println("Please enter a list of integers: ");
        // taking user input as a string
        Scanner in = new Scanner(System.in);
        String userString = in.nextLine();
        // converting user string into string array of words
        String[] userArray = userString.split(" ");
        // initializing int array
        int[] numArray = new int[userArray.length];
        // initializing index for alternating sign
        int sign = -1;
        // initializing variable result
        int result = 0;
        // converting string array to int array
        // computing alternating sum of int array
        for (int i = 0; i < userArray.length; i++) {
            numArray[i] = Integer.parseInt(userArray[i]);
            sign *= -1;                         // alternating sign
            result += numArray[i] * (sign);     // adding each int in numArray with alternating sign

        }
        // printing alternating sum result
        System.out.println("The alternating sum of those numbers is: " + result);
    }
}
